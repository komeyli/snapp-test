package com.example.snapp.test.client;

import java.util.Map;

import com.example.snapp.test.client.model.ProviderInquiryCellNumberResponse;
import com.example.snapp.test.exception.ClientException;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;


@Component
@FeignClient(name = "shahkarProviderClient", url = "${shahkar.provider.base.url}")
public interface ShahkarProviderClient {

	@GetMapping(path = "/shahkar/verify", produces = MediaType.APPLICATION_JSON_VALUE)
	ProviderInquiryCellNumberResponse shahkarVerify(@RequestParam Map<String, String> params) throws ClientException;

}
