package com.example.snapp.test.client.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class ProviderInquiryCellNumberResponse {

	@JsonProperty("isValid")
	private boolean verified;

	private String trackId;

}
