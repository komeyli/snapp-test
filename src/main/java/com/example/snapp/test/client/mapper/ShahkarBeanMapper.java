package com.example.snapp.test.client.mapper;

import com.example.snapp.test.utilize.ResultStatus;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;

import org.springframework.stereotype.Component;

@Component
@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE, imports = ResultStatus.class)
public interface ShahkarBeanMapper {


}
