package com.example.snapp.test.data.dao;

import com.example.snapp.test.data.ShahkarRequestJournal;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ShahkarRequestJournalDao extends MongoRepository<ShahkarRequestJournal, String> {

}
