package com.example.snapp.test.data;

import java.util.stream.Stream;

import com.example.snapp.test.data.model.User;
import com.example.snapp.test.utilize.ResultStatus;
import lombok.Getter;
import lombok.Setter;

import org.springframework.data.annotation.Version;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.mapping.Document;

@Getter
@Setter
@Document(collection = "shahkar_request_journals")
@CompoundIndexes({ @CompoundIndex(name = "date", def = "{ 'date': -1 }", background = true),
		@CompoundIndex(name = "trackingCode", def = "{ 'trackingCode': 1 }", background = true, unique = true) })
public class ShahkarRequestJournal extends MongoBaseEntity {

	private String trackingCode;

	private Status status;

	private Long date;

	private ResultStatus result;

	private String originalResponse;

	private User user;

	@Version
	private int version;

	public enum Status implements IntEnumConvertable {

		VERIFIED(0), UNVERIFIED(1), FAILURE(2);

		private final int value;

		Status(int value) {
			this.value = value;
		}

		public static Status fromValue(int value) {
			return Stream.of(Status.values()).filter(status -> status.getValue() == value).findFirst()
					.orElseThrow(() -> new IllegalStateException("undefined status found " + value));
		}

		public int getValue() {
			return value;
		}

	}

}
