package com.example.snapp.test.data.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class User {

	private String userId;

	private String cellNumber;

	private String firstName;

	private String lastName;

	private String nationalCode;


	public static User of(String userId) {
		User user = new User();
		user.setUserId(userId);
		return user;
	}

}
