package com.example.snapp.test.service.impl;

import java.util.Map;

import com.example.snapp.test.client.ShahkarProviderClient;
import com.example.snapp.test.client.model.ProviderInquiryCellNumberResponse;
import com.example.snapp.test.data.ShahkarRequestJournal;
import com.example.snapp.test.data.dao.ShahkarRequestJournalDao;
import com.example.snapp.test.exception.ClientException;
import com.example.snapp.test.service.ShahkarService;
import com.example.snapp.test.service.mapper.ShahkarServiceBeanMapper;
import com.example.snapp.test.service.model.ShahkarProviderModel;
import com.example.snapp.test.service.model.ShahkarProviderResult;
import com.example.snapp.test.service.model.ShahkarVerifyRequestParam;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;

import org.springframework.stereotype.Component;


@Component
@RequiredArgsConstructor
public class ShahkarServiceImpl implements ShahkarService {

	private final ShahkarProviderClient shahkarProviderClient;

	private final ObjectMapper objectMapper;

	private final ShahkarServiceBeanMapper mapper;

	private final ShahkarRequestJournalDao shahkarRequestJournalDao;

	@Override
	public ShahkarProviderResult verify(ShahkarProviderModel model) throws ClientException {
		ShahkarVerifyRequestParam shahkarProviderRequest = mapper.toShahkarVerifyRequestParam(model);
		Map<String, String> request = objectMapper.convertValue(shahkarProviderRequest, Map.class);
		ProviderInquiryCellNumberResponse respone = shahkarProviderClient.shahkarVerify(request);
		ShahkarRequestJournal dataModel = mapper.toShahkarRequestJournal(respone, model);
		shahkarRequestJournalDao.save(dataModel);
		return mapper.toShahkarProviderResult(respone);
	}

}
