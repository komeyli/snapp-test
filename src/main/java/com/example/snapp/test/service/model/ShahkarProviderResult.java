package com.example.snapp.test.service.model;

import java.io.Serial;
import java.io.Serializable;

import com.example.snapp.test.utilize.ResultStatus;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class ShahkarProviderResult implements Serializable {

	@Serial
	private static final long serialVersionUID = -2611103692441055322L;

	private String trackingCode;

	private boolean fetchedFromCache;

	private boolean verified;

	private ResultStatus result;

	private String originalResponse;

}
