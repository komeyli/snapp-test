package com.example.snapp.test.service;

import com.example.snapp.test.exception.BusinessException;
import com.example.snapp.test.service.model.ShahkarProviderModel;
import com.example.snapp.test.service.model.ShahkarProviderResult;

public interface ShahkarService {

	ShahkarProviderResult verify(ShahkarProviderModel request) throws BusinessException;

}
