package com.example.snapp.test.service.mapper;

import com.example.snapp.test.client.model.ProviderInquiryCellNumberResponse;
import com.example.snapp.test.data.ShahkarRequestJournal;
import com.example.snapp.test.service.model.ShahkarProviderModel;
import com.example.snapp.test.service.model.ShahkarProviderResult;
import com.example.snapp.test.service.model.ShahkarVerifyRequestParam;
import com.example.snapp.test.utilize.ResultStatus;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;

import org.springframework.stereotype.Component;

@Component
@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE, imports = ResultStatus.class)
public interface ShahkarServiceBeanMapper {

	@Mapping(target = "mobile", source = "cellNumber")
	@Mapping(target = "nationalCode", source = "nationalCode")
	ShahkarVerifyRequestParam toShahkarVerifyRequestParam(ShahkarProviderModel model);

	ShahkarProviderResult toShahkarProviderResult(ProviderInquiryCellNumberResponse response);

	ShahkarRequestJournal toShahkarRequestJournal(ProviderInquiryCellNumberResponse response, ShahkarProviderModel model);
}
