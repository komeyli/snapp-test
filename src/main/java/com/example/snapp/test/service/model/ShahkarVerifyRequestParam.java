package com.example.snapp.test.service.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class ShahkarVerifyRequestParam {

	private String mobile;

	private String nationalCode;

}
