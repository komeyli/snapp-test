package com.example.snapp.test.service.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@EqualsAndHashCode
public class ShahkarProviderModel {

	@EqualsAndHashCode.Exclude
	private String initiator;

	@EqualsAndHashCode.Exclude
	private String userId;

	private String cellNumber;

	private String nationalCode;

	private boolean forceInquiry;

}
