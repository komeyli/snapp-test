package com.example.snapp.test.api.web.shahkar.model;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

import lombok.Data;

@Data
public class CellNumberInquiryRequest {

	@NotBlank
	@ApiModelProperty(position = 0, required = true, example = "091216458541",
			notes = "The field represents the cellNumber of user")
	@Pattern(regexp = "^(09)\\d{9}")
	private String cellNumber;

	@NotBlank
	@ApiModelProperty(position = 0, required = true, example = "01451232184",
			notes = "The field represents the national code of user")
	private String nationalCode;

}
