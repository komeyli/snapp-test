package com.example.snapp.test.api.web.shahkar;

import javax.validation.Valid;

import com.example.snapp.test.api.web.shahkar.mapper.ShahkarResourceMapper;
import com.example.snapp.test.api.web.shahkar.model.CellNumberInquiryRequest;
import com.example.snapp.test.api.web.shahkar.model.CellNumberInquiryResponse;
import com.example.snapp.test.exception.BusinessException;
import com.example.snapp.test.service.ShahkarService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@Slf4j
@RestController
@RequestMapping("/kyc")
@RequiredArgsConstructor
public class ShahkarResource {

	private final ShahkarResourceMapper mapper;

	private final ShahkarService shahkarService;

	@PostMapping(path = "/cell-number/inquiry", consumes = MediaType.APPLICATION_JSON_VALUE,
			produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<CellNumberInquiryResponse> inquiryCellNumber(
			@RequestHeader(value = "User-id", required = false) String userId,
			@Valid @RequestBody CellNumberInquiryRequest request) throws BusinessException {
		var model = mapper.toCellNumberInquiryRequestModel(request, userId);
		var result = shahkarService.verify(model);
		return ResponseEntity.ok(mapper.toCellNumberInquiryResponse(result));
	}

}
