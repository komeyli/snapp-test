package com.example.snapp.test.api.web.shahkar.model;

import com.example.snapp.test.utilize.ResultStatus;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class CellNumberInquiryResponse {

	private boolean verified;

	private ResultStatus result;

}
