package com.example.snapp.test.api.web.shahkar.mapper;

import com.example.snapp.test.api.web.shahkar.model.CellNumberInquiryRequest;
import com.example.snapp.test.api.web.shahkar.model.CellNumberInquiryResponse;
import com.example.snapp.test.service.model.ShahkarProviderModel;
import com.example.snapp.test.service.model.ShahkarProviderResult;
import com.example.snapp.test.utilize.ResultStatus;
import org.mapstruct.BeanMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", imports = { ResultStatus.class })
public interface ShahkarResourceMapper {

	@BeanMapping(ignoreByDefault = true)
	@Mapping(target = "userId", source = "userId")
	@Mapping(target = "cellNumber", source = "request.cellNumber")
	@Mapping(target = "nationalCode", source = "request.nationalCode")
	ShahkarProviderModel toCellNumberInquiryRequestModel(CellNumberInquiryRequest request, String userId);

	@Mapping(target = "result", expression = "java(ResultStatus.SUCCESS)")
	CellNumberInquiryResponse toCellNumberInquiryResponse(ShahkarProviderResult response);

}
