package com.example.snapp.test.exception;

import com.example.snapp.test.utilize.ResultStatus;
import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class ClientException extends BusinessException {

	private static final long serialVersionUID = 3247387297698727005L;

	protected final ResultStatus resultStatus;

	private final String responseCode;

	private final String responseMessage;

	public ClientException(ResultStatus resultStatus, String responseCode, String responseMessage) {
		super(responseCode);
		this.responseCode = responseCode;
		this.resultStatus = resultStatus;
		this.responseMessage = responseMessage;
	}

	@Override
	public ResultStatus getResultStatus() {
		return resultStatus;
	}

}
