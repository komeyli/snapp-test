package com.example.snapp.test.utilize;

public enum ResultStatus {
	SUCCESS(0, "success"),
	UNKNOWN(1, "unknown.error"),
	ENCRYPTION_ERROR(2, "encryption.error");

	private final String description;
	private final Integer status;

	ResultStatus(int status, String description) {
		this.status = status;
		this.description = description;
	}
}
